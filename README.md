# Passwordgenerator

- Sie benoetigen [Python 3](https://www.python.org/downloads/windows/)!
- Ausfuerbares Python Script mit GUI ist die datei `main.py`!
- Ausfuerbares Python Script ohne GUI ist die datei `gen.py`!
- Den installer, der Python 3 und den Passwortgenerator automatisch installiert, finden sie [Hier](https://bitbucket.org/Sharkbyteprojects/passwordgenerator/downloads/Passwortgenerator_installer.exe) und im [Downloadbereich](https://bitbucket.org/Sharkbyteprojects/passwordgenerator/downloads/) des Repositories! (Falls das AV Programm alarm schlaegt, ist dies nur eie warnung, weil unsere software nicht signiert ist!)
- [GitHub Gist](https://gist.github.com/Sharkbyteprojects/4227cb27db4eaa36dbab879f22073513)